#include <iostream>
using std::cout;
using std::endl;
using std::cin;
class Stack
{
public:
    Stack()
    {
        cout << "Enter the stack size: ";
        cin >> stackSize;
        array = new int[stackSize];
    }
    ~Stack()
    {
        cout << "Name: Stack\n"
            << "Delete from memory\n"
            << "     * *       "
            << endl;
        delete[]array;
    }
    void Push(int a)
    {
        if (lastIndex < stackSize)
        {
            array[++lastIndex] = a;
        }
        else
        {
            cout << "Stack is full\n";
        }
    }
    void Pop()
    {
        if (lastIndex > -1)
        {
            cout << array[lastIndex] << endl;
            array[lastIndex--] = 0;
        }
        else
        {
            std::cout << "Stack is empty!" << endl;
        }
    }
    void ShowArray()
    {
        if (lastIndex > -1)
        {
            for (int i = 0; i <= lastIndex; i++)
            {
                cout << "[" << i << "] = " << array[i] << endl;
            }
        }
        else
        {
            cout << "Stack is empty!" << endl;
        }
    }
    void FillRandom()
    {
        for (int i = 0; i < stackSize; i++)
        {
            lastIndex = i;
            array[i] = rand();
        }
    }
private:
    int stackSize;
    int lastIndex = -1;
    int* array;
};
int main()
{
    Stack s;
    s.FillRandom();
    s.ShowArray();
    s.Pop();
    s.Pop();
    s.Pop();
    s.ShowArray();
    return 0;
}